using System;
using UnityEngine;

namespace LittleFighterII.GamePlay
{
	[Serializable]
	public enum CommandType {
		None,
		Left,
		Right,
		Up,
		Down,
		Defence,
		Attack,
		Jump,
	}

	[Serializable]
	public class Command {
		public CommandType Type;
		public float StartTime;
		public float EndTime;
	}

	[Serializable]
	public class CharacterControlInputSetting {
		public KeyCode Left = KeyCode.LeftArrow;
		public KeyCode Right = KeyCode.RightArrow;
		public KeyCode Up = KeyCode.UpArrow;
		public KeyCode Down = KeyCode.DownArrow;
		public KeyCode Defense = KeyCode.Z;
		public KeyCode Attack = KeyCode.C;
		public KeyCode Jump = KeyCode.X;
	}
}