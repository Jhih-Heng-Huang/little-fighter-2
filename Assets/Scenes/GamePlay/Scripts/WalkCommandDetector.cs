using System;
using System.Collections;
using UnityEngine;

namespace LittleFighterII.GamePlay
{
	public class WalkCommandDetector
	{
		#region Events
		public event Action OnWalkLeft;
		public event Action OnWalkRight;
		public event Action OnWalkUp;
		public event Action OnWalkDown;
		#endregion

		#region Private members
		private readonly CharacterControlInputSetting _setting;
		private bool _isRunning = false;
		#endregion

		public WalkCommandDetector(CharacterControlInputSetting setting) {
			_setting = setting;
		}

		#region Public methods
		public IEnumerator Detect()
		{
			_isRunning = true;
			while(_isRunning)
			{
				if (IsWalkLeft()) OnWalkLeft?.Invoke();
				yield return null;
				if (IsWalkRight()) OnWalkRight?.Invoke();
				yield return null;
				if (IsWalkUp()) OnWalkUp?.Invoke();
				yield return null;
				if (IsWalkDown()) OnWalkDown?.Invoke();
				yield return null;
			}
		}

		public void Stop()
		{
			_isRunning = false;
		}
		#endregion

		#region Public methods
		public bool IsWalkLeft() {
			return _IsPressed(_setting.Left);
		}
		
		public bool IsWalkRight() {
			return _IsPressed(_setting.Right);
		}

		public bool IsWalkUp() {
			return _IsPressed(_setting.Up);
		}

		public bool IsWalkDown() {
			return _IsPressed(_setting.Down);
		}
		#endregion

		#region Private methods
		private bool _IsPressed(KeyCode code) {
			return Input.GetKey(code) || Input.GetKeyDown(code);
		}

		private bool _IsStopPressed(KeyCode code) {
			return Input.GetKeyUp(code);
		}
		#endregion
	}
}
