﻿using System;
using System.Collections;

using UnityEngine;
using UniRx;

using Rayark.Mast;

namespace LittleFighterII.GamePlay
{
	public partial class TemplateCharacterController
	{

		private class RunState : ICharacterState
		{
			#region Private members
			private TemplateCharacterController _controller;
			private Vector2 _moveDirection;
			private bool _isRunning;
			#endregion

			public RunState(TemplateCharacterController controller, Vector2	moveDirection)
			{
				_controller = controller;
				_controller._processQueue.Enqueue(_Run());

				_moveDirection = moveDirection;
			}

			#region Protected methods
			protected override void _Register(TemplateCharacterController controller)
			{
				if (_IsMovingToRight())
				{
					_disposes.AddRange(new IDisposable[]{
						controller._keyDownAttackStream.Subscribe(_ => _EnterDashAttack(Vector2.right)),
					});
					
					controller._walkAndRunKeyStream.OnKeyDownLeftKey += _EnterBrakeState;
					_defer.Add(() => controller._walkAndRunKeyStream.OnKeyDownLeftKey -= _EnterBrakeState);
				}
				else
				{
					_disposes.AddRange(new IDisposable[]{
						controller._keyDownAttackStream.Subscribe(_ => _EnterDashAttack(Vector2.left)),
					});
					controller._walkAndRunKeyStream.OnKeyDownRightKey += _EnterBrakeState;
					_defer.Add(() => controller._walkAndRunKeyStream.OnKeyDownRightKey -= _EnterBrakeState);
				}
				_defer.Add(() => _isRunning = false);
			}
			#endregion

			#region Private methods
			private IEnumerator _Run()
			{
				_Register(_controller);
				_controller._view.RotateBody(_moveDirection);
				yield return _Running();
			}
			private void _EnterBrakeState()
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_BRAKE);
				_controller._state = new BrakeState(_controller, _moveDirection.x);
			}
			private void _EnterDashAttack(Vector2 dir)
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_DASH_ATTACK);
				_controller._state = new DashAttackState(_controller, dir);
			}
			private bool _IsMovingToRight()
			{
				return _moveDirection.x > 0;
			}
			private IEnumerator _Running()
			{
				_isRunning = true;
				var main = new Executor();
				main.Add(_controller._WaitAnimationEnd(AnimationState.Run));
				main.Add(_RunningMove());
				yield return main.Join();
			}
			private IEnumerator _RunningMove()
			{
				var xDir = _IsMovingToRight() ? 1 : -1;
				var view = _controller._view;
				var bodyPos = view.BodyPosition;
				var runSpeed = view.RunSpeed;
				while (_isRunning)
				{
					var deltaTime = Time.deltaTime;
					var xyDir = new Vector2(xDir, _DetectYInput()).normalized;
					bodyPos.x += runSpeed * xyDir.x * deltaTime;
					bodyPos.y += runSpeed * xyDir.y * deltaTime; 
					view.BodyPosition = bodyPos;
					yield return null;
				}
			}
			private float _DetectYInput()
			{
				var y = 0f;
				var inputSetting = _controller._inputSetting;
				if (Input.GetKey(inputSetting.Up)) y += 1;
				if (Input.GetKey(inputSetting.Down)) y -= 1;
				return y;
			}
			#endregion
		}

		private class BrakeState: ICharacterState
		{
			#region Private members
			private TemplateCharacterController _controller;
			private float _xdir;
			#endregion

			public BrakeState(TemplateCharacterController controller, float xDir)
			{
				_controller = controller;
				_controller._processQueue.Enqueue(_Run());
				_xdir = xDir;
			}

			#region Private methods
			private IEnumerator _Run()
			{
				_Register(_controller);
				yield return _Brake();
				_Unregister();
				_controller._state = new IdleState(_controller);
			}
			private IEnumerator _Brake()
			{
				var main = new Executor();
				main.Add(_controller._WaitAnimationEnd(AnimationState.Brake));
				main.Add(_BrakeMove());
				yield return main.Join();
			}
			private IEnumerator _BrakeMove()
			{
				var xDir = _xdir > 0 ? 1 : -1;
				var view = _controller._view;
				var bodyPos = view.BodyPosition;
				var runSpeed = view.RunSpeed;
				var brakeAcc = view.BrakeOfRunAcc;

				while (runSpeed > 0)
				{
					var deltaTime = Time.deltaTime;
					bodyPos.x += runSpeed * xDir * deltaTime;
					runSpeed -= brakeAcc * deltaTime;
					view.BodyPosition = bodyPos;
					yield return null;
				}
			}
			#endregion
		}
	}
}
