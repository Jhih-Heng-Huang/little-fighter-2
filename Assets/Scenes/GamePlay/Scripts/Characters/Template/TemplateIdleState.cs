﻿using System;
using System.Collections;

using UniRx;
using UnityEngine;

namespace LittleFighterII.GamePlay
{
	public partial class TemplateCharacterController
	{
		private class IdleState : ICharacterState
		{
			private TemplateCharacterController _controller;

			public IdleState(TemplateCharacterController controller)
			{
				_controller = controller;
				_controller._processQueue.Enqueue(_Run());
			}

			protected override void _Register(TemplateCharacterController controller)
			{
				_disposes.AddRange(
					new IDisposable[]{
						controller._keyDownAttackStream.Subscribe(_ => _TriggerAttack()),
						controller._keyOnDefenceStream.Subscribe(_ => _TriggerDefenceState()),
						controller._keyDownJumpStream.Subscribe(_ => _EnterJumpState()),
					}
				);
				
				controller._walkAndRunKeyStream.OnKeyDownLeftKey += _EnterWalkState;
				controller._walkAndRunKeyStream.OnKeyDownRightKey += _EnterWalkState;
				controller._walkAndRunKeyStream.OnKeyDownUpKey += _EnterWalkState;
				controller._walkAndRunKeyStream.OnKeyDownDownKey += _EnterWalkState;
				controller._walkAndRunKeyStream.OnTriggerLeftRun += _EnterLeftRunState;
				controller._walkAndRunKeyStream.OnTriggerRightRun += _EnterRightRunState;
				
				_defer.Add(() =>
				{
					controller._walkAndRunKeyStream.OnKeyDownLeftKey -= _EnterWalkState;
					controller._walkAndRunKeyStream.OnKeyDownRightKey -= _EnterWalkState;
					controller._walkAndRunKeyStream.OnKeyDownUpKey -= _EnterWalkState;
					controller._walkAndRunKeyStream.OnKeyDownDownKey -= _EnterWalkState;
					controller._walkAndRunKeyStream.OnTriggerLeftRun -= _EnterLeftRunState;
					controller._walkAndRunKeyStream.OnTriggerRightRun -= _EnterRightRunState;
				});
			}

			#region Private methods
			private IEnumerator _Run()
			{
				_Register(_controller);
				yield return _controller._WaitAnimationEnd(AnimationState.IDLE);
			}
			private void _TriggerAttack()
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_ATTACK_1);
				_controller._state = new Attack1State(_controller);
			}
			private void _TriggerDefenceState()
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_DEFENCE);
				_controller._state = new DefenceState(_controller);
			}
			private void _EnterWalkState()
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_WALK);
				_controller._state = new WalkState(_controller);
			}
			private void _EnterJumpState()
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_JUMP);
				_controller._state = new ReadyToJumpState(_controller);
			}
			private void _EnterLeftRunState()
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_RUN);
				_controller._state = new RunState(_controller, Vector2.left);
			}
			private void _EnterRightRunState()
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_RUN);
				_controller._state = new RunState(_controller, Vector2.right);
			}
			#endregion
		}
	}
}