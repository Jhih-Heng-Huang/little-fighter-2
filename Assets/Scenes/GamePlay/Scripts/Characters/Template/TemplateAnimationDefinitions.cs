﻿namespace LittleFighterII.GamePlay
{
	public partial class TemplateCharacterController
	{
		private static class AnimationState
		{
			public const string IDLE = "Idle";
			public const string ATTACK_1 = "Attack_1";
			public const string ATTACK_2 = "Attack_2";
			public const string ATTACK_3 = "Attack_3";
			public const string DEFENCE = "Defence";
			public const string WALK = "Walk";
			public const string ReadyToJump = "ReadyToJump";
			public const string JumpOffFloor = "JumpOffFloor";
			public const string Run = "Run";
			public const string Brake = "Brake";
			public const string ChargeForDashAttack = "ChargeForDashAttack";
			public const string DashAttack = "DashAttack";
			public const string EndOfDashAttack = "EndOfDashAttack";
		}
		private static class AnimationTrigger
		{
			public const string TO_ATTACK_1 = "to_Attack_1";
			public const string TO_ATTACK_2 = "to_Attack_2";
			public const string TO_ATTACK_3 = "to_Attack_3";
			public const string TO_DEFENCE = "to_Defence";
			public const string TO_IDLE = "to_Idle";
			public const string TO_WALK = "to_Walk";
			public const string TO_JUMP = "to_Jump";
			public const string TO_RUN = "to_Run";
			public const string TO_BRAKE = "to_Brake";
			public const string TO_DASH_ATTACK = "to_Dash_Attack";
		}
	}
}
