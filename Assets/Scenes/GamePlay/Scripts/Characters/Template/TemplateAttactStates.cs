﻿using System.Collections;
using UniRx;

namespace LittleFighterII.GamePlay
{
	public partial class TemplateCharacterController
	{
		private class Attack1State : ICharacterState
		{
			private TemplateCharacterController _controller;
			public Attack1State(TemplateCharacterController controller)
			{
				_controller = controller;
				_controller._processQueue.Enqueue(_Run());

				_Register(_controller);
			}

			protected override void _Register(TemplateCharacterController controller)
			{
				var dispose = controller._keyDownAttackStream.Subscribe(_ => _TriggerAttack());
				_disposes.Add(dispose);
			}

			private IEnumerator _Run()
			{
				yield return _controller._WaitAnimationEnd(AnimationState.ATTACK_1);
				if (!_controller._IsStateName(AnimationState.IDLE))
					yield break;
				_Unregister();
				_controller._state = new IdleState(_controller);
			}

			private void _TriggerAttack()
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_ATTACK_2);
				_controller._state = new Attack2State(_controller);
			}
		}

		private class Attack2State : ICharacterState
		{
			private TemplateCharacterController _controller;
			public Attack2State(TemplateCharacterController controller)
			{
				_controller = controller;
				_controller._processQueue.Enqueue(_Run());

				_Register(_controller);
			}

			protected override void _Register(TemplateCharacterController controller)
			{
				var dispose = controller._keyDownAttackStream.Subscribe(_ => _TriggerAttack());
				_disposes.Add(dispose);
			}

			private IEnumerator _Run()
			{
				yield return _controller._WaitAnimationEnd(AnimationState.ATTACK_2);
				if (!_controller._IsStateName(AnimationState.IDLE))
					yield break;
				_Unregister();
				_controller._state = new IdleState(_controller);
			}

			private void _TriggerAttack()
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_ATTACK_3);
				_controller._state = new Attack3State(_controller);
			}
		}

		private class Attack3State : ICharacterState
		{
			private TemplateCharacterController _controller;
			public Attack3State(TemplateCharacterController controller)
			{
				_controller = controller;
				_controller._processQueue.Enqueue(_Run());
			}

			private IEnumerator _Run()
			{
				yield return _controller._WaitAnimationEnd(AnimationState.ATTACK_3);
				_controller._state = new IdleState(_controller);
			}
		}
	}
}
