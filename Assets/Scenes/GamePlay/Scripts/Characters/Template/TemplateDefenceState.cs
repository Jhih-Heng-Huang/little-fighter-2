﻿using System;
using System.Collections;

using UnityEngine;
using UniRx;

namespace LittleFighterII.GamePlay
{
	public partial class TemplateCharacterController
	{
		private class DefenceState : ICharacterState
		{
			private TemplateCharacterController _controller;
			public DefenceState(TemplateCharacterController controller)
			{
				_controller = controller;
				_controller._processQueue.Enqueue(_Run());
			}

			protected override void _Register(TemplateCharacterController controller)
			{
				_disposes.AddRange(
					new IDisposable[]{
						controller._keyUpDefenceStream.Subscribe(_ => _TriggerIdleState()),
					}
				);
				controller._OnTriggerDefenceLeftAttack += _EnterLeftDashAttackState;
				controller._OnTriggerDefenceRightAttack += _EnterRightDashAttackState;
				controller._walkAndRunKeyStream.OnKeyDownLeftKey += controller._FaceToLeft;
				controller._walkAndRunKeyStream.OnKeyDownRightKey += controller._FaceToRight;
				_defer.Add(() =>
				{
					controller._OnTriggerDefenceLeftAttack -= _EnterLeftDashAttackState;
					controller._OnTriggerDefenceRightAttack -= _EnterRightDashAttackState;
					controller._walkAndRunKeyStream.OnKeyDownLeftKey -= controller._FaceToLeft;
					controller._walkAndRunKeyStream.OnKeyDownRightKey -= controller._FaceToRight;
				});
			}

			private IEnumerator _Run()
			{
				_Register(_controller);
				yield return _controller._WaitAnimationEnd(AnimationState.DEFENCE);
			}

			private void _TriggerIdleState()
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_IDLE);
				_controller._state = new IdleState(_controller);
			}
			private void _EnterLeftDashAttackState()
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_DASH_ATTACK);
				_controller._state = new DashAttackState(_controller, Vector2.left);
			}
			private void _EnterRightDashAttackState()
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_DASH_ATTACK);
				_controller._state = new DashAttackState(_controller, Vector2.right);
			}
		}
	}
}
