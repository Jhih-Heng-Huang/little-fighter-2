﻿using System;
using System.Collections;

using System.Linq;

using UnityEngine;

using UniRx;
using UniRxExtension;

namespace LittleFighterII.GamePlay
{
	public partial class TemplateCharacterController
	{
		private class WalkState: ICharacterState
		{
			#region Private members
			private TemplateCharacterController _controller;
			private IObservable<long> _noXYInputStream;
			private bool _isWalking;
			#endregion

			public WalkState(TemplateCharacterController controller)
			{
				_isWalking = false;
				_controller = controller;
				_controller._PushProcess(_Run());
				_Register(controller);
			}

			protected override void _Register(TemplateCharacterController controller)
			{
				_noXYInputStream = Observable.EveryUpdate()
					.Where(_ =>
					new KeyCode[]
					{
						controller._inputSetting.Left,
						controller._inputSetting.Right,
						controller._inputSetting.Up,
						controller._inputSetting.Down,
					}.All(key => !Input.GetKey(key)));
				_disposes.AddRange(
					new IDisposable[]
					{
						_noXYInputStream.Subscribe(_ => _EnterIdleState()),
						controller._keyDownJumpStream.Subscribe(_ => _EnterJumpState()),
					}
				);
				_defer.Add(() => _isWalking = false);
			}

			private IEnumerator _Run()
			{
				_isWalking = true;
				while (_isWalking)
				{
					var direction = _DetectXYInputDirection();
					_MoveXYOn(direction);
					_controller._view.RotateBody(direction);
					yield return null;
				}
			}
			private void _EnterIdleState()
			{
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_IDLE);
				_controller._state = new IdleState (_controller);
			}
			private void _EnterJumpState()
			{
				_Unregister();
				var xyDirection = _DetectXYInputDirection();

				_controller._SetTrigger(AnimationTrigger.TO_JUMP);
				_controller._state = new ReadyToJumpState(_controller, xyDirection);
			}
			private Vector2 _DetectXYInputDirection()
			{
				var direction = Vector2.zero;

				bool detectKey(KeyCode key) => Input.GetKey(key) || Input.GetKeyDown(key);

				if (detectKey(_controller._inputSetting.Left)) direction += Vector2.left;
				if (detectKey(_controller._inputSetting.Right)) direction += Vector2.right;
				if (detectKey(_controller._inputSetting.Up)) direction += Vector2.up;
				if (detectKey(_controller._inputSetting.Down)) direction += Vector2.down;

				return direction;
			}
			private void _MoveXYOn(Vector2 xyDirection)
			{
				var pos = _controller._view.BodyPosition;
				var moveSpeed = _controller._view.MoveSpeed;
				var deltaTime = Time.deltaTime;
				pos.x += xyDirection.x * moveSpeed * deltaTime;
				pos.y += xyDirection.y * moveSpeed * deltaTime;
				_controller._view.BodyPosition = pos;
			}
		}
	}
}
