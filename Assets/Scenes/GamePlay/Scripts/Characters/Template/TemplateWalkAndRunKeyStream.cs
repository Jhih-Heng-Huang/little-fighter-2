﻿using System;
using System.Collections;
using System.Collections.Generic;

using Rayark.Mast;

using UniRx;
using UnityEngine;
using UniRxExtension;

namespace LittleFighterII.GamePlay
{
	public partial class TemplateCharacterController
	{
		private class WalkAndRunKeyStream : IDisposable
		{
			#region Events
			public event Action OnKeyDownLeftKey;
			public event Action OnKeyDownRightKey;
			public event Action OnKeyDownUpKey;
			public event Action OnKeyDownDownKey;
			public event Action OnTriggerLeftRun;
			public event Action OnTriggerRightRun;
			#endregion

			#region Streams
			private IObservable<KeyCode> _keyDownLeftStream;
			private IObservable<KeyCode> _keyDownRightStream;
			private IObservable<KeyCode> _keyDownUpStream;
			private IObservable<KeyCode> _keyDownDownStream;
			private IObservable<KeyCode> _rightRunKeyComboStream;
			private IObservable<KeyCode> _leftRunKeyComboStream;
			#endregion

			#region Constants
			private const float PERIOD = 250;
			#endregion

			#region Private members
			private CharacterControlInputSetting _inputSetting;
			private Defer _defer = new Defer();
			#endregion

			public WalkAndRunKeyStream(CharacterControlInputSetting inputSetting)
			{
				_inputSetting = inputSetting;
			}

			#region Public methods
			public void Init()
			{
				_keyDownLeftStream = KeyStream.GenKeyDownStream(_inputSetting.Left);
				_keyDownRightStream = KeyStream.GenKeyDownStream(_inputSetting.Right);
				_keyDownUpStream = KeyStream.GenKeyDownStream(_inputSetting.Up);
				_keyDownDownStream = KeyStream.GenKeyDownStream(_inputSetting.Down);

				_GenLeftRunKeyComboStream();
				_GenRightRunKeyComboStream();

				var keyDownUpStreamDispose = _keyDownUpStream.Subscribe(_ => OnKeyDownUpKey?.Invoke());
				var keyDownDownStreamDispose = _keyDownDownStream.Subscribe(_ => OnKeyDownDownKey?.Invoke());

				_defer.Add(keyDownUpStreamDispose.Dispose);
				_defer.Add(keyDownDownStreamDispose.Dispose);
			}
			public void Dispose()
			{
				_defer.Dispose();
			}
			#endregion

			#region Private methods
			private void _GenLeftRunKeyComboStream()
			{
				IDisposable dispose = null;
				dispose = _keyDownLeftStream.Subscribe(_0 =>
				{
					OnKeyDownLeftKey?.Invoke();
					_leftRunKeyComboStream = KeyStream.GenKeyActionInTimeStream(PERIOD, Input.GetKeyDown, _inputSetting.Left);
					_leftRunKeyComboStream.Subscribe(_1 => OnTriggerLeftRun?.Invoke(), () => _GenLeftRunKeyComboStream());
					dispose?.Dispose();
				});
			}
			private void _GenRightRunKeyComboStream()
			{
				IDisposable dispose = null;
				dispose = _keyDownRightStream.Subscribe(_0 =>
				{
					OnKeyDownRightKey?.Invoke();
					_rightRunKeyComboStream = KeyStream.GenKeyActionInTimeStream(PERIOD, Input.GetKeyDown, _inputSetting.Right);
					_rightRunKeyComboStream.Subscribe(_1 => OnTriggerRightRun?.Invoke(), () => _GenRightRunKeyComboStream());
					dispose?.Dispose();
				});
			}
			#endregion
		}
	}
}
