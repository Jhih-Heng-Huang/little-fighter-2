﻿using System.Collections;
using UnityEngine;

namespace LittleFighterII.GamePlay
{
	public partial class TemplateCharacterController
	{
		private class ReadyToJumpState : ICharacterState
		{
			private TemplateCharacterController _controller;
			private Vector2 _xyDirection;
			public ReadyToJumpState(TemplateCharacterController controller, Vector2 xyDirection=default(Vector2))
			{
				_controller = controller;
				_controller._processQueue.Enqueue(_Run());

				_xyDirection = xyDirection;

				_Register(_controller);
			}

			private IEnumerator _Run()
			{
				yield return _controller._WaitAnimationEnd(AnimationState.ReadyToJump);
				_Unregister();
				_controller._state = new JumpOffFloorState(_controller, _xyDirection);
			}
		}

		private class JumpOffFloorState: ICharacterState
		{
			private TemplateCharacterController _controller;
			private Vector2 _xyDirection;

			public JumpOffFloorState(TemplateCharacterController controller, Vector2 xyDirection=default(Vector2))
			{
				_controller = controller;
				_controller._processQueue.Enqueue(_Run());
				_xyDirection = xyDirection;

				_Register(_controller);
			}

			#region Private methods
			private IEnumerator _Run()
			{
				yield return _Jump();
				_Unregister();
				_controller._SetTrigger(AnimationTrigger.TO_IDLE);
				_controller._state = new IdleState(_controller);
			}
			private IEnumerator _Jump()
			{
				var bodyPos = _controller._view.BodyPosition;
				var jumpSpeed = _GenJumpSpeedVector(_controller._view.JumpSpeed);
				var gravity = _controller._view.Gravity;

				do
				{
					var deltaTime = Time.deltaTime;

					bodyPos.x += jumpSpeed.x * deltaTime;
					bodyPos.y += jumpSpeed.y * deltaTime;
					bodyPos.z += jumpSpeed.z * deltaTime;
					
					jumpSpeed.z -= gravity * deltaTime;

					_controller._view.BodyPosition = bodyPos;
					yield return null;
				} while (bodyPos.z > 0f);

				bodyPos.z = 0f;
				_controller._view.BodyPosition = bodyPos;
			}
			private Vector3 _GenJumpSpeedVector(float jumpSpeed)
			{
				var speedVector = new Vector3();
				speedVector.x = _xyDirection.x;
				speedVector.y = _xyDirection.y;
				speedVector.z = 1;
				speedVector = speedVector.normalized * jumpSpeed;
				return speedVector;
			}
			#endregion
		}

	}
}
