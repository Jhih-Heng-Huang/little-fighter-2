using System.Collections;

using UnityEngine;

using Rayark.Mast;

namespace LittleFighterII.GamePlay
{
	public class TemplateView: MonoBehaviour
	{
		#region Serialized Fields
		[SerializeField]
		private RectTransform _xyRectTransform;
		[SerializeField]
		private RectTransform _bodyRectTransform;
		[SerializeField]
		private Animator _bodyAnimator;
		[SerializeField]
		private float _moveSpeed;
		[SerializeField]
		private float _jumpSpeed;
		[SerializeField]
		private float _gravity;
		[SerializeField]
		private float _runSpeed;
		[SerializeField]
		private float _brakeOfRunAcc;
		[SerializeField]
		private float _brakeOfDashAttactAcc;
		#endregion

		#region Public Fields
		public RectTransform XYRectTransform { get => _xyRectTransform; }
		public Animator BodyAnimator { get => _bodyAnimator;  }
		public float MoveSpeed { get => _moveSpeed; }
		public float JumpSpeed { get => _jumpSpeed; }
		public float Gravity { get => _gravity; }
		public float RunSpeed { get => _runSpeed; }
		public float BrakeOfRunAcc { get => _brakeOfRunAcc; }
		public float BrakeOfDashAttackAcc { get => _brakeOfDashAttactAcc; }
		public Vector3 BodyPosition { get => _GetBodyPosition(); set => _SetBodyPosition(value); }
		#endregion

		#region Public methods
		public void RotateBody(Vector2 direction)
		{
			if (direction.x == 0) return;
			_xyRectTransform.rotation = (direction.x > 0) ? Quaternion.Euler(0, 0, 0) : Quaternion.Euler(0, 180, 0);
		}
		#endregion

		#region Private methods
		private Vector3 _GetBodyPosition()
		{
			var pos = Vector3.zero;
			pos.x = _xyRectTransform.anchoredPosition.x;
			pos.y = _xyRectTransform.anchoredPosition.y / 0.7f;
			pos.z = _bodyRectTransform.anchoredPosition.y;
			return pos;
		}
		private void _SetBodyPosition(Vector3 pos)
		{
			_xyRectTransform.anchoredPosition = new Vector2(pos.x, pos.y * 0.7f);
			_bodyRectTransform.anchoredPosition = new Vector2(0, pos.z);
		}
		#endregion
	}
}