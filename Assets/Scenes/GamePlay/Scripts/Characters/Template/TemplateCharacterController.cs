using System;
using System.Collections;
using System.Collections.Generic;

using Rayark.Mast;

using UniRx;
using UnityEngine;
using UniRxExtension;

namespace LittleFighterII.GamePlay
{
	using Engine;
	public partial class TemplateCharacterController
	{
		#region Private subclasses
		private abstract class ICharacterState
		{
			protected List<IDisposable> _disposes = new List<IDisposable>();
			protected Defer _defer = new Defer();

			protected virtual void _Register(TemplateCharacterController controller)
			{}

			protected void _Unregister()
			{
				foreach (var disposable in _disposes)
					disposable.Dispose();
				_disposes.Clear();
				_defer.Dispose();
			}
		}
		#endregion

		#region Constants
		private const float _KEY_COMBINE_DURATION_MILLISEC = 1000;
		#endregion

		#region Private members
		private ICharacterState _state = null;
		private ProcessQueue _processQueue = new ProcessQueue();
		private TemplateView _view;
		private CharacterControlInputSetting _inputSetting = null;
		private WalkAndRunKeyStream _walkAndRunKeyStream;
		#endregion

		#region Events
		private event Action _OnTriggerDefenceLeftAttack;
		private event Action _OnTriggerDefenceRightAttack;
		#endregion

		#region Key Streams
		private IObservable<KeyCode> _keyUpDefenceStream = null;
		private IObservable<KeyCode> _keyOnDefenceStream = null;
		private IObservable<KeyCode> _keyDownAttackStream = null;
		private IObservable<KeyCode> _keyDownJumpStream = null;
		private IObservable<KeyCode> _keyDownDefenceLeftAttackStream;
		private IObservable<KeyCode> _keyDownDefenceRightAttackStream;
		#endregion

		public TemplateCharacterController(TemplateView view, CharacterControlInputSetting inputSetting)
		{
			_view = view;
			_inputSetting = inputSetting;

			_GenKeyStreams(inputSetting);
			_state = new IdleState(this);

			_walkAndRunKeyStream = new WalkAndRunKeyStream(inputSetting);
			_walkAndRunKeyStream.Init();
		}

		#region Public methods
		public IEnumerator Run()
		{
			yield return _processQueue.Run();
		}
		#endregion

		#region Private methods
		private IEnumerator _WaitAnimationEnd(string stateName)
		{
			yield return _view.BodyAnimator.WaitAnimationEnd(stateName);
		}
		private void _FaceToLeft() => _view.RotateBody(Vector2.left);
		private void _FaceToRight() => _view.RotateBody(Vector2.right);
		private bool _IsStateName(string name) => _view.BodyAnimator.IsStateName(name);
		private void _SetTrigger(string name) => _view.BodyAnimator.SetTrigger(name);
		private void _PushProcess(IEnumerator process) => _processQueue.Enqueue(process);
		private void _GenKeyStreams(CharacterControlInputSetting inputSetting)
		{
			_keyUpDefenceStream = KeyStream.GenKeyUpStream(inputSetting.Defense);
			_keyOnDefenceStream = KeyStream.GenKeyOnStream(inputSetting.Defense);

			_keyDownAttackStream = KeyStream.GenKeyDownStream(inputSetting.Attack);
			_keyDownJumpStream = KeyStream.GenKeyDownStream(inputSetting.Jump);

			_keyDownDefenceLeftAttackStream =
				KeyStream.GenKeyDownStream(inputSetting.Defense)
				.BindKeyStream(_KEY_COMBINE_DURATION_MILLISEC, Input.GetKeyDown, inputSetting.Left)
				.BindKeyStream(_KEY_COMBINE_DURATION_MILLISEC, Input.GetKeyDown, inputSetting.Attack);

			_keyDownDefenceRightAttackStream =
				KeyStream.GenKeyDownStream(inputSetting.Defense)
				.BindKeyStream(_KEY_COMBINE_DURATION_MILLISEC, Input.GetKeyDown, inputSetting.Right)
				.BindKeyStream(_KEY_COMBINE_DURATION_MILLISEC, Input.GetKeyDown, inputSetting.Attack);

			_keyDownDefenceLeftAttackStream.Subscribe(_ => _OnTriggerDefenceLeftAttack?.Invoke());
			_keyDownDefenceRightAttackStream.Subscribe(_ => _OnTriggerDefenceRightAttack?.Invoke());
		}
		#endregion
	}

}