﻿using Rayark.Mast;

using System.Collections;

using UnityEngine;

namespace LittleFighterII.GamePlay
{
	public partial class TemplateCharacterController
	{
		private class DashAttackState : ICharacterState
		{
			private Vector2 _moveDir;
			private TemplateCharacterController _controller;
			public DashAttackState(TemplateCharacterController controller, Vector2 moveDir)
			{
				_controller = controller;
				_controller._processQueue.Enqueue(_Run());

				_moveDir = moveDir;
			}

			private IEnumerator _Run()
			{
				var process = new Executor();
				process.Add(_RunAnimationAndAttackTarget());
				process.Add(_RunBrakeMove());
				yield return process.Join();
				_controller._state = new IdleState(_controller);
			}
			private IEnumerator _RunAnimationAndAttackTarget()
			{
				yield return _controller._WaitAnimationEnd(AnimationState.ChargeForDashAttack);
				yield return _controller._WaitAnimationEnd(AnimationState.DashAttack);
				Debug.Log("Hit rival!");
				yield return _controller._WaitAnimationEnd(AnimationState.EndOfDashAttack);
			}
			private IEnumerator _RunBrakeMove()
			{
				var xDir = _moveDir.x;
				var view = _controller._view;
				var bodyPos = view.BodyPosition;
				var runSpeed = view.RunSpeed;
				var brakeAcc = view.BrakeOfDashAttackAcc;

				while (runSpeed > 0)
				{
					var deltaTime = Time.deltaTime;
					bodyPos.x += runSpeed * xDir * deltaTime;
					runSpeed -= brakeAcc * deltaTime;
					view.BodyPosition = bodyPos;
					yield return null;
				}
			}
		}
	}
}
