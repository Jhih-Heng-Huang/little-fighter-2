using UnityEngine;
using UnityEngine.UI;

namespace Rayark.Cytus2
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(Graphic))]
	public class AnalogGlitchGraphicEffect : MonoBehaviour
	{
		#region Public Properties

		// Scan line jitter

		[SerializeField, Range(0, 1)]
		float _scanLineJitter = 0;

		public float ScanLineJitter {
			get { return _scanLineJitter; }
			set { _scanLineJitter = value; }
		}

		// Vertical jump

		[SerializeField, Range(0, 1)]
		float _verticalJump = 0;

		public float VerticalJump {
			get { return _verticalJump; }
			set { _verticalJump = value; }
		}

		// Horizontal shake

		[SerializeField, Range(0, 1)]
		float _horizontalShake = 0;

		public float HorizontalShake {
			get { return _horizontalShake; }
			set { _horizontalShake = value; }
		}


		// Color drift

		[SerializeField, Range(0, 1)]
		float _colorDrift = 0;

		public float colorDrift {
			get { return _colorDrift; }
			set { _colorDrift = value; }
		}
		#endregion

		#region Private Properties
		Graphic _graphic;

		float _verticalJumpTime;

		#endregion

		#region MonoBehaviour Functions
		private void Awake()
		{
			_graphic = GetComponent<Graphic>();
			_graphic.material = new Material(Shader.Find("Glitch/AnalogEffect"));
		}

		private void Update()
		{
			_verticalJumpTime += Time.deltaTime * _verticalJump * 11.3f;

			var sl_thresh = Mathf.Clamp01(1.0f - _scanLineJitter * 1.2f);
			var sl_disp = 0.002f + Mathf.Pow(_scanLineJitter, 3) * 0.05f;
			_graphic.materialForRendering.SetVector("_ScanLineJitter", new Vector2(sl_disp, sl_thresh));

			var vj = new Vector2(_verticalJump, _verticalJumpTime);
			_graphic.materialForRendering.SetVector("_VerticalJump", vj);

			_graphic.materialForRendering.SetFloat("_HorizontalShake", _horizontalShake * 0.2f);

			var cd = new Vector2(_colorDrift * 0.04f, Time.time * 606.11f);
			_graphic.materialForRendering.SetVector("_ColorDrift", cd);
		}

		private void OnDestroy() 
		{
			if(_graphic.material != null)
				Destroy(_graphic.material);
		}
		#endregion
	}
}