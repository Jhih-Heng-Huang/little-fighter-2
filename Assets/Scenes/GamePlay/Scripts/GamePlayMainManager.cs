using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rayark.Mast;


namespace LittleFighterII.GamePlay
{	
	public class GamePlayMainManager: MonoBehaviour {
		
		#region Serialize Fields
		[SerializeField]
		private TemplateView _templateView;
		#endregion

		#region Private members
		private Executor _mainRunner = new Executor();
		private List<CharacterController> _controllers = new List<CharacterController>();
		#endregion

		#region Mono Behaviours
		private void Start() {
			_mainRunner.Add(_Main());
		}
		private void Update() {
			if (!_mainRunner.Finished)
				_mainRunner.Resume(Rayark.Mast.Coroutine.Delta);
		}
		#endregion

		#region Public methods
		public IEnumerator _Main () {
			var controller = new TemplateCharacterController(_templateView, new CharacterControlInputSetting());
			yield return controller.Run();
		}
		#endregion

		#region Private methods
		#endregion
	}
}