﻿Shader "Glitch/Jitter"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Jitter ("Jitter", Range(0,1)) = 0
	}
	SubShader
	{
		// No culling or depth
		Cull Off
		ZWrite Off

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			float _Jitter;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color: COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 color: COLOR;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.color = v.color;
				return o;
			}

			float nrand(float x, float y)
			{
				return frac(sin(dot(float2(x, y), float2(12.9898, 78.233))) * 43758.5453);
			}

			fixed4 frag (v2f i) : SV_Target
			{
				float jitter = (nrand(i.uv.y, _Time.x) * 2 - 1) * _Jitter;
				fixed4 color = tex2D(_MainTex, frac(float2(i.uv.x + jitter, i.uv.y)));
				return color * i.color;
			}
			ENDCG
		}
	}
}
