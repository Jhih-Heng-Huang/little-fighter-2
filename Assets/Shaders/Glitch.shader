﻿Shader "Glitch/AnalogEffect"
{
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Tint", Color) = (1, 1, 1, 1)
	}
	
	SubShader {
		Tags
		{
			"Queue"="Transparent"
			"RenderType"="Transparent"
			"IgnoreProjector"="True"
			"PreviewType"="Plane"
		}
		Cull Off
		Lighting Off
		ZWrite Off
		
		
		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
	
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#include "UnityCG.cginc"
			
			uniform sampler2D _MainTex;
			fixed4 _Color;

			float2 _MainTex_TexelSize;
			float2 _ScanLineJitter; // (displacement, threshold)
			float2 _VerticalJump;   // (amount, time)
			float _HorizontalShake;
			float2 _ColorDrift;     // (amount, time)
			
			struct appdata {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
			};
			
			struct v2f {
				float4 position : SV_POSITION;
				float2 texcoord : TEXCOORD0;
				fixed4 color : COLOR;
			};

			
		   	v2f vert(appdata v) {
				v2f o;
				o.position = UnityObjectToClipPos(v.vertex);
				o.texcoord = v.texcoord;
				o.color = v.color;
				return o;
			}
			
			float nrand(float x, float y)
			{
				return frac(sin(dot(float2(x, y), float2(12.9898, 78.233))) * 43758.5453);
			}

			fixed4 frag(v2f i) : SV_Target{
				float u = i.texcoord.x;
				float v = i.texcoord.y;

				// Scan line jitter
				float jitter = nrand(v, _Time.x) * 2 - 1;
				jitter *= step(_ScanLineJitter.y, abs(jitter)) * _ScanLineJitter.x;

				// Vertical jump
				float jump = lerp(v, frac(v + _VerticalJump.y), _VerticalJump.x);

				// Horizontal shake
				float shake = (nrand(_Time.x, 2) - 0.5) * _HorizontalShake;

				// Color drift
				float drift = sin(jump + _ColorDrift.y) * _ColorDrift.x;

				fixed4 src1 = tex2D(_MainTex, frac(float2(u + jitter + shake, jump)));
				fixed4 src2 = tex2D(_MainTex, frac(float2(u + jitter + shake + drift, jump)));
				
				return fixed4(src1.r, src2.g, src1.b, src1.a) * i.color;
			}
			ENDCG
		}

	}
	FallBack "Sprites/Default"
}
